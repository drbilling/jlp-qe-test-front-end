import { test, expect } from '@playwright/test';
 
 // Expect the page of dishwashers to be displayed, and that the first object is visible to the user
  test('test', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await expect(page.getByRole('link', { name: 'Bosch Serie 2 SMV40C30GB' })).toBeVisible();

// Given additional time, I would like to have parameterised this test
// or been able to assert the content/presence of dynamic data such as the 'name:' 
// or iterate over a provided set of test data in a JSON file for example.

});