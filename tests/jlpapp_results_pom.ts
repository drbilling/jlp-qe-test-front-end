import { expect, type Locator, type Page } from '@playwright/test';

export class jlpappresultspom {
    readonly pagetitle
}

// If I had more time I would have started to build a page object model for the search results, product information and the error page.
// This would allow the tests to be more maintainable, as the app grew and progressed