import { test, expect } from '@playwright/test';

// Check to verify the correct error text when a Product cannot be located

    test('test', async ({ page }) => {
        await page.goto("http://localhost:3000/product-detail/0123456789");
        await expect(page.locator('h2')).toContainText('That product doesn\'t seem to exist');
        await expect(page.getByRole('paragraph')).toContainText('Go back to the Homepage');
});