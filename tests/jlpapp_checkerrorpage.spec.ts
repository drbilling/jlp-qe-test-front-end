import { test, expect } from '@playwright/test';

    test('test', async ({ page }) => {
        await page.goto("http://localhost:3000/'");
        await expect(page.locator('h2')).toContainText('That page cannot be found');
        await expect(page.getByRole('paragraph')).toContainText('Go back to the Homepage');
});