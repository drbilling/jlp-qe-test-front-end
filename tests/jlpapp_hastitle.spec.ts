import { test, expect } from '@playwright/test';

test('has title', async ({ page }) => {
  await page.goto('http://localhost:3000');

  // Expect a title "to contain" a substring.
  // Checks to asser the page has loaded and the page title is correcxt
  await expect(page).toHaveTitle(/JL & Partners \| Home/);

});
