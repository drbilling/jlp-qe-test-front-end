import { test, expect } from '@playwright/test';

  test('test', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.getByRole('link', { name: 'Bosch Serie 2 SMV40C30GB' }).click();
    await expect(page.locator('.product-carousel_productCarousel__mC0GE')).toBeVisible();
    await expect(page.locator('h1')).toContainText('Bosch Serie 2 SMV40C30GB Fully Integrated Dishwasher');
    await expect(page.getByText('£')).toBeVisible();
    await expect(page.getByRole('heading', { name: 'Bosch Serie 2 SMV40C30GB' })).toBeVisible();
    await expect(page.getByText('Product informationThe Bosch')).toBeVisible();
    await expect(page.getByText('Product specificationTypeIntegratedNoise Level50dBRinse Aid')).toBeVisible();
  });

// Given additional time, I would like to have parameterised this test
// or been able to assert the content/presence of dynamic data such as product data and specifications
// or iterate over a provided set of test data in a JSON file for example.
