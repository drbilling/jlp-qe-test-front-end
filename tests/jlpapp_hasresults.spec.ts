import { test, expect } from '@playwright/test';
 
 // Expect the page of dishwashers to be displayed
  test('test', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await expect(page.getByRole('heading')).toContainText(/Dishwashers \(.*\)/);

});